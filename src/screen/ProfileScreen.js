import React from "react";
import { useSelector } from "react-redux";
import Button from "../Common/Button/Button";
import { selectuser } from "../features/userSlice";
import { auth } from "../firebase";
import Nav from "../Nav";
import "./ProfileScreen.css";

const ProfileScreen = () => {
  const user = useSelector(selectuser);
  return (
    <div className="profileScreen">
      <Nav />
      <div className="profileScreen__body">
        <h1>Edit Profile</h1>
        <div className="profileScreen__info">
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSknZa7BMPr3Q5a-weAgSJ6oopsp117egIdsg&usqp=CAU" />
          <div className="profileScreen__details">
            <h2>{user.email}</h2>
            <div className="profileScreen__plans">
              <h3>Plans</h3>
              <Button
                className="profileScreen__signout"
                value="Sign out"
                onClick={() => auth.signOut()}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileScreen;
