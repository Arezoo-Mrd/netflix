/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import SignUpScreen from "./SignUpScreen";
import Button from "../Common/Button/Button";
import "./LoginScreen.css";

const LoginScreen = () => {
  const [signIn, setSignIn] = useState(false);

  return (
    <div className="loginScreen">
      <div className="loginScreen__background">
        <img
          className="loginScreen__logo"
          src="https://upload.wikimedia.org/wikipedia/commons/7/7a/Logonetflix.png"
        />
        <Button
          className="loginScreen__button"
          value={"Sign In"}
          onClick={() => setSignIn(true)}
        />

        <div className="logoScreen__gradient" />
      </div>
      <div className="loginScreen__body">
        {signIn ? (
          <SignUpScreen />
        ) : (
          <>
            <h1>Unlimited films, TV programmes and more.</h1>
            <h2>Watch anywhere. Cancel at any time.</h2>
            <h3>
              Ready to watch? Enter your email to create or restart your
              membership.
            </h3>
            <div className="loginScreen__input">
              <form onSubmit={() => console.log("yes")}>
                <input type="email" placeholder="Email Address" />
                <Button
                  className="loginScreen__getStarted"
                  value={" Get Started"}
                  onClick={() => setSignIn(true)}
                />
              </form>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default LoginScreen;
