import React, { useRef } from "react";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import Button from "../Common/Button/Button";
import { auth } from "../firebase";
import "./SignUpScreen.css";

const SignUpScreen = () => {
  const emailRef = useRef();
  const passwordRef = useRef();

  const register = (e) => {
    e.preventDefault();

    createUserWithEmailAndPassword(
      auth,
      emailRef.current.value,
      passwordRef.current.value
    )
      .then((authUser) => {
        console.log(authUser);
      })
      .catch((error) => {
        console.log("error in register: ", error.message);
      });
  };

  const signIn = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(
      auth,
      emailRef.current.value,
      passwordRef.current.value
    )
      .then((authUser) => {
        console.log("sign In authUser: ", authUser);
      })
      .catch((error) => {
        console.log("error in sign In: ", error.message);
      });
  };

  return (
    <div className="signUpScreen">
      <form>
        <h1>Sign In</h1>
        <input
          ref={emailRef}
          type={"email"}
          placeholder="Email Or phone number"
          name="email"
        />
        <input
          ref={passwordRef}
          type="password"
          placeholder="Password"
          name="password"
        />
        <Button
          className="signUpScreen__button"
          value={"Sign In"}
          onClick={signIn}
        />
        <h4>
          <span className="signUpScreen__gray">New to Netflix?</span>
          <span className="signUpScreen__link" onClick={register}>
            Sign up now.
          </span>
        </h4>
      </form>
    </div>
  );
};

export default SignUpScreen;
