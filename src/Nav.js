import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Nav.css";

const Nav = () => {
  const [show, setShow] = useState(false);

  const transitonNavBar = () => {
    if (window.scrollY > 100) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", transitonNavBar);
    return () => {
      window.removeEventListener("scroll", transitonNavBar);
    };
  }, []);

  return (
    <nav className={`nav ${show && "nav__black"}`}>
      <div className="nav__contents">
        <Link to="/">
          <img
            className="nav__logo"
            src="https://upload.wikimedia.org/wikipedia/commons/7/7a/Logonetflix.png"
          />
        </Link>
        <Link to="/profile">
          <img
            className="nav__avatar"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSknZa7BMPr3Q5a-weAgSJ6oopsp117egIdsg&usqp=CAU"
          />
        </Link>
      </div>
    </nav>
  );
};

export default Nav;
