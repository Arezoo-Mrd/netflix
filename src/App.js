import { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import Login from "./screen/LoginScreen";
import HomeScreen from "./HomeScreen";
import { auth } from "./firebase";
import { login, logout, selectuser } from "./features/userSlice";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import ProfileScreen from "./screen/ProfileScreen";

function App() {
  const dispatch = useDispatch();
  const user = useSelector(selectuser);
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((userAuth) => {
      if (userAuth) {
        dispatch(
          login({
            uid: userAuth.uid,
            email: userAuth.email,
          })
        );
      } else {
        dispatch(logout());
      }
    });
    return unsubscribe();
  }, [dispatch]);

  return (
    <div className="app">
      {!user ? (
        <Login />
      ) : (
        <Routes>
          <Route path="/profile" element={<ProfileScreen />} />
          <Route path="/" element={<HomeScreen />} />
          <Route path="/test" element={<h1>This is Test</h1>} />
        </Routes>
      )}
    </div>
  );
}

export default App;
