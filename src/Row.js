import React, { useEffect, useState } from "react";
import http from "./httpServices";
import "./Row.css";

const Row = ({ title, fetchUrl, isLargerRow = false }) => {
  const [movies, setMovies] = useState([]);

  const base_url = "https://image.tmdb.org/t/p/original/";

  useEffect(() => {
    async function fetchData() {
      const { data } = await http.get(fetchUrl);
      setMovies(data.results);
    }
    fetchData();
  }, [fetchUrl]);

  return (
    <div className="row">
      <h2>{title}</h2>
      <div className="row__posters">
        {movies.map(
          (movie) =>
            ((isLargerRow && movie.poster_path) ||
              (!isLargerRow && movie.backdrop_path)) && (
              <img
                key={movie.id}
                className={`row__poster ${isLargerRow && "row_posterLarger"}`}
                src={`${base_url}${
                  isLargerRow ? movie.poster_path : movie.backdrop_path
                }`}
                alt={movie.name}
              />
            )
        )}
      </div>
    </div>
  );
};

export default Row;
