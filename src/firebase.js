import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore/lite";

const firebaseConfig = {
  apiKey: "AIzaSyDIAKAVmI2JmuOCnNSqKZVInn7NVGwhqf8",
  authDomain: "netflix-clone-65b3e.firebaseapp.com",
  projectId: "netflix-clone-65b3e",
  storageBucket: "netflix-clone-65b3e.appspot.com",
  messagingSenderId: "1029934512573",
  appId: "1:1029934512573:web:f349bcb09a096d7e417ad4",
};

const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth();

export { auth };
export default db;
